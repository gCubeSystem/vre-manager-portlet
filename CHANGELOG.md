
# Changelog for VRE Manager Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v6.1.0] - 2021-10-13

- Added possibility to postpone VRE expiration time of 5 years

## [v5.3.0] - 2015-04-28
 - Feature #849, added possibility to Undeploy a VRE, 
 - Added possibility to postpone VRE expiration time of six months
 - Ported to GWT 2.7.0

## [v5.0.0] - 2013-10-21

- Ported to Feather Weight Stack
- Removed GCF Dependency
- Logging with sl4j Enabled
